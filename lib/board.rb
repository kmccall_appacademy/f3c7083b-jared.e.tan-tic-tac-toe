class Board

  attr_accessor :grid

  def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
  end

  def place_mark(pos, mark)
    return 'error' unless pos.all? {|i| (0..2).include?(i)}
    return 'error' unless mark == :X || mark == :O || mark == nil
    row = pos[0]
    col = pos[1]
    @grid[row][col] = mark
  end

  def empty?(pos)
    return true if @grid[pos[0]][pos[1]].nil?
    false
  end

  def winner
    return rows unless rows.nil?
    return cols unless cols.nil?
    return diagonal unless diagonal.nil?
    nil
  end

  def rows
    @grid.each do |i2|
      return i2[0] if i2.uniq.length == 1 && !i2[0].nil?
    end
    nil
  end

  def cols
    transposed_grid = @grid.transpose
    transposed_grid.each do |i3|
      return i3[0] if i3.uniq.length == 1 && !i3[0].nil?
    end
    nil
  end

  def diagonal
    left_diag = [@grid[0][0], @grid[1][1], @grid[2][2]]
    right_diag = [@grid[0][2], @grid[1][1], @grid[2][0]]
    return left_diag[0] if left_diag.uniq.length == 1 && !left_diag[0].nil?
    return right_diag[0] if right_diag.uniq.length == 1 && !right_diag[0].nil?
    nil
  end

  def over?
    nil_count = 0
    @grid.each { |i| nil_count += i.count(nil) }
    return true if nil_count.zero?
    return true unless winner.nil?
    false
  end

end
