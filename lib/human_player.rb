class HumanPlayer

  attr_accessor :name

  def initialize(name)
    @name = name
  end

  def get_move(game = Board.new, _)
    print "#{name}, where?: "
    array_input = convert_input(gets.chomp)
      until valid_move?(game, array_input)
        print 'Please enter a valid move ex) 0, 0 : '
        array_input = convert_input(gets.chomp)
      end
    array_input
  end

  def convert_input(input)
    chars = input.chars
    numbers = chars.select { |i| '0123456789'.include?(i) }
    numbers.map(&:to_i)
  end

  def valid_move?(game, pos)
    return false unless pos.length == 2
    return false unless pos.all? { |i| (0..2).include?(i) }
    return false unless game.empty?(pos)
    true
  end

  def display(board)
    print board.grid
  end

end
