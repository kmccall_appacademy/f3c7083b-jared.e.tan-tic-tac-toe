require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
# Game!
class Game

  attr_accessor :board, :player_one, :player_two

  def initialize(player_one, player_two)
    @position = 0
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @players = {:X => player_one, :O => player_two}
  end

  def play_turn
    mark = :X
    mark = :O if @position.odd?
    game = @board
    @board.place_mark(current_player.get_move(game, mark), mark)
    switch_players!
  end

  def current_player
    return player_one if @position.even?
    player_two
  end

  def switch_players!
    @position += 1
  end

  def play
    until @board.over?
      play_turn
      @board.grid.each do |r|
        puts r.each { |p| p }.join(" ")
      end
      print "#{player_two.name}'s move \n" if @position.odd? && !@board.over?
    end

    if @board.winner
      puts "#{@players[@board.winner].name} is the winner!"
    else
      puts "No one wins"
    end
  end



  if __FILE__ == $PROGRAM_NAME
    h = HumanPlayer.new("Jard")
    c = ComputerPlayer.new("Hanzo")

    Game.new(h, c).play
  end


end
