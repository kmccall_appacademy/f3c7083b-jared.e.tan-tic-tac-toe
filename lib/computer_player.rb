class ComputerPlayer

  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move(game = Board.new, mark)
    original = game.dup
    (0..2).each do |i|
      (0..2).each do |i2|
        start_empty = false
        answer = nil
        if original.empty?([i, i2])
          original.place_mark([i, i2], mark)
          start_empty = true
        end
        answer = [i, i2] if original.winner
        original.place_mark([i, i2], nil) if start_empty
        return answer unless answer.nil?
      end
    end
    get_random_move(game)
  end

  def get_random_move(game)
    original = game.dup
    move = [rand(3), rand(3)]
    until original.empty?(move)
      move = [rand(3), rand(3)]
    end
    move
  end

end
